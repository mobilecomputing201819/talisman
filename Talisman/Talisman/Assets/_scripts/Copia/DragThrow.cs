﻿using UnityEngine;
using System.Collections;

public class DragThrow : MonoBehaviour
{
    //valori da salvare per vettore
    Vector2 startPos;
    Vector2 endPos;
    Vector2 d;      //vettore risultante di startpos e endpos

    //variabili pubbliche
    public float max = 2.0f;    //normalizzatore in fixedLaunch
    public bool thrown;     //verifica se la copia è stata lanciata o no
    public float speed = 50.0f; //moltiplicatore di velocità al lancio
    public float delay = 0.7f;  //dopo quanto interrompere il lancio
    public bool buttonPressed;  //per evitare che legga la posizione del dito sopra i pulsanti
    private Animator anim;
    public AudioClip ChargeClip;
    public AudioClip ThrowClip;

    Camera camera;  //la camera principale
    LineRenderer lr;    //per disegnare la direzione mentre si trascina il mouse
    Vector3 camOffset = new Vector3(0, 0, 10);  //per il lineRenderer


    void Start()
    {
        camera = Camera.main;
        anim = GetComponent<Animator>();
        lr = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.touchCount > 0) && !buttonPressed)	//se tocca col dito
        {
            Touch touch = Input.GetTouch(0);    //segui questo dito

            switch (touch.phase)	//dai istruzioni diverse dipendendo dalla fase del tocco
            {
                //caso iniziale
                case TouchPhase.Began:
                    lr.enabled = true;  //attiva il line renderer
                    lr.positionCount = 2;   //due posizioni di ancoraggio del lineRenderer, essenzialmente fai una linea
                    startPos = camera.ScreenToWorldPoint(touch.position) + camOffset;  //posizione iniziale è dove hai cliccato col mouse
                    lr.SetPosition(0, transform.position);
                    lr.useWorldSpace = true;    //rendilo visibile
                    GetComponent<AudioSource>().clip = ChargeClip;
                    GetComponent<AudioSource>().loop = true;
                    GetComponent<AudioSource>().Play();
                    break;

                //caso di drag
                case TouchPhase.Moved:
                    endPos = camera.ScreenToWorldPoint(touch.position) + camOffset;  //posizione finale è dove si trova mouse durante il drag
                    modifyEndPos();
                    d = fixedLaunch(d); //normalizza il vettore direzionale
                                        //ridisegna la linea a ogni update
                    lr.SetPosition(0, transform.position);
                    lr.SetPosition(1, transform.position + new Vector3(d.x, d.y, 0));
                    break;

                //caso rilascio
                case TouchPhase.Ended:
                    lr.enabled = false;     //disattiva il line renderer
                                            //var force = fixedLaunch(endPos- startPos);
                    GetComponent<AudioSource>().Stop();
                    GetComponent<AudioSource>().loop = false;
                    GetComponent<AudioSource>().clip = ThrowClip;
                    GetComponent<AudioSource>().Play();
                    GameObject.FindWithTag("Player").GetComponent<CapsuleCollider2D>().enabled = false;
                    GameObject.FindWithTag("Player").GetComponent<CircleCollider2D>().enabled = true;
                    GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);       //azzera la velocità della copia così non si somma al lancio
                    GetComponent<Rigidbody2D>().mass = 0.7f;
                    GetComponent<ConstantForce2D>().force = d * speed;  //attiva la constant force con questo valore       
                    anim.SetBool("Thrown", true);
                    thrown = true;  //è stato lanciato
                    StartCoroutine(waiter());
                    break;
            }
        }

        if (Input.GetMouseButtonDown(0) && !buttonPressed)    //se clicchi il mouse
        {
            lr.enabled = true;  //attiva il line renderer
            lr.positionCount = 2;   //due posizioni di ancoraggio del lineRenderer, essenzialmente fai una linea
            startPos = camera.ScreenToWorldPoint(Input.mousePosition) + camOffset;  //posizione iniziale è dove hai cliccato col mouse
            lr.SetPosition(0, transform.position);
            lr.useWorldSpace = true;    //rendilo visibile
            GetComponent<AudioSource>().clip = ChargeClip;
            GetComponent<AudioSource>().loop = true;
            GetComponent<AudioSource>().Play();
        }
        if (Input.GetMouseButton(0) && !buttonPressed)    //se tieni premuto il mouse
        {

            endPos = camera.ScreenToWorldPoint(Input.mousePosition) + camOffset;  //posizione finale è dove si trova mouse durante il drag
            modifyEndPos();
            d = fixedLaunch(d); //normalizza il vettore direzionale
            //ridisegna la linea a ogni update
            lr.SetPosition(0, transform.position);
            lr.SetPosition(1, transform.position + new Vector3(d.x, d.y, 0));
        }
        if (Input.GetMouseButtonUp(0) && !buttonPressed)   //se rilasci il mouse
        {
            lr.enabled = false;     //disattiva il line renderer
            //var force = fixedLaunch(endPos- startPos);
            GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().loop = false;
            GetComponent<AudioSource>().clip = ThrowClip;
            GetComponent<AudioSource>().Play();
            GameObject.FindWithTag("Player").GetComponent<CapsuleCollider2D>().enabled = false;
            GameObject.FindWithTag("Player").GetComponent<CircleCollider2D>().enabled = true;
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);       //azzera la velocità della copia così non si somma al lancio
            GetComponent<Rigidbody2D>().mass = 0.7f;
            GetComponent<ConstantForce2D>().force = d * speed;  //attiva la constant force con questo valore       
            anim.SetBool("Thrown", true);
            thrown = true;  //è stato lanciato
            StartCoroutine(waiter());
        }

        if (buttonPressed)
        {
            buttonPressed = false;
        }
    }


    void OnCollisionStay2D(Collision2D coll)
    {
        if ((coll.gameObject.tag == "Wall" || coll.gameObject.tag == "Enemy") && thrown) Time.timeScale = 1;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Enemy") GameObject.FindWithTag("Player").GetComponent<LivesAndCloneSpawning>().allowed = false;
        //StopCoroutine(GameObject.FindWithTag("Player").GetComponent<PlayerAbilities>().lastCoroutine);
    }
    //algoritmo che fa in modo che la freccia sia nella direzione del lancio invece che dove è stato toccato e dove viene trascinato il mouse
    void modifyEndPos()
    {
        d = new Vector2(startPos.x - endPos.x, startPos.y - endPos.y);  //crea vettore direzionale pronto per il lancio
        endPos = endPos + d * 2;    //aggiustalo così che la punta del vettore indica la direzione del lancio, non del trascinamento
    }

    //classe che attende un dato tempo ed azzera la forza costante e la velocità del corpo per interrompere la spinta
    IEnumerator waiter()
    {
        yield return new WaitForSeconds(delay);
        GetComponent<ConstantForce2D>().force = new Vector2();  //disattiva la constant force
        GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x / 10, 0);   //disattiva la velocità verticale e rallenta quella orizzontale così fa tipo parabola
        thrown = false; //è possibile rilanciare
    }

    //classe che prende un vettore direzione e ne restituisce uno con la stessa direzione e un'intensità costante 
    public Vector2 fixedLaunch(Vector2 dir)
    {
        var x = 1;
        var y = 1;
        if (dir.x < 0)  //verifica se viene lanciato indietro
        {
            x = -1;
        }
        if (dir.y < 0)  //verifica se viene lanciato in basso
        {
            y = -1;
        }
        var angle = dir.y / dir.x;  //calcola coefficiente
        if (x < 0 && y < 0)    //se dir punta indietro in basso (quindi dividendo le componenti il valore è positivo), rendilo di nuovo negativo
        {
            angle = -angle;
        }
        if (angle <= 1 && angle >= -1)    //semplice trigonometria (non toccare)
        {
            return new Vector2(x * max, y * max * Mathf.Abs(angle));
        }
        return new Vector2(x * max / Mathf.Abs(angle), y * max);
    }
}




















