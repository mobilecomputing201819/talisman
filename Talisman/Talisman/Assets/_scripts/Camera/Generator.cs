﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour {

    public GameObject screen;
    public Transform middlePoint;
    public Transform generationPoint;
    public Transform deletionPoint;
    public Transform oldMiddlePoint;
    private float offSet = 10;
	// Use this for initialization
	void Start () {
        Instantiate(screen, new Vector3(middlePoint.position.x, middlePoint.position.y, 0), middlePoint.rotation);
    }
	
	// Update is called once per frame
	void Update () {
		if(generationPoint.position.y > middlePoint.position.y)
        {          
            middlePoint.position = new Vector2(middlePoint.position.x, middlePoint.position.y + offSet);
            Instantiate(screen, new Vector3(middlePoint.position.x, middlePoint.position.y, 0), middlePoint.rotation);
        }

        if(deletionPoint.position.y > oldMiddlePoint.position.y)
        {

            oldMiddlePoint.position = new Vector2(oldMiddlePoint.position.x, oldMiddlePoint.position.y + offSet);

        }
     
	}
}
