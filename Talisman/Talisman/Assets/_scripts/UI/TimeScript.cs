﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeScript : MonoBehaviour
{

    Text text;
    float timer;
    string seconds;
    string minutes;

    public void Start()
    {
        text = GetComponent<Text>();
        timer = 0;
    }

    void Update()
    {
        timer += Time.deltaTime;

        minutes = Mathf.Floor(timer / 60).ToString("00");
        seconds = (timer % 60).ToString("00");

        text.text = "Time: " + minutes + ":" + seconds;
    }
}