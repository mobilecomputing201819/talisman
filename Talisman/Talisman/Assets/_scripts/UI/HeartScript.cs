﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeartScript : MonoBehaviour {

    public int life;   //valore di vita per cui il cuore scompare
    GameObject player;  //il player

	// Use this for initialization
	void Start ()
    {
        player = GameObject.FindWithTag("Player");
    }
	
	// Update is called once per frame
	void Update ()
    { 
        //se le vite del player scendono al valore di life o se muore per qualunque ragione, distruggi questo cuore
        if ((player.GetComponent<LivesAndCloneSpawning>().lives == life) || (player == null))
        {
            Destroy(gameObject);
        }
	}
}
