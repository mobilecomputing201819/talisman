﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockCamera : MonoBehaviour {

    Camera camera;

	void Start () {
        camera = Camera.main;
	}

    //script che disattiva il movimento della camera quando si aprono le opzioni
    public void Options()
    {
        camera.GetComponent<CameraMovement>().enabled = false;  //se stai aprendo le opzioni, ferma la camera
    }
}
