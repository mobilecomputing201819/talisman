﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartCamera : MonoBehaviour {

    Camera camera;

    void Start()
    {
        camera = Camera.main;
    }

    //script che riattiva il movimento della camera quando si chiudono le opzioni
    public void Options()
    {
        camera.GetComponent<CameraMovement>().enabled = true;   //se stai chiudendo le opzioni, fai ripartire la camera
    }
}
