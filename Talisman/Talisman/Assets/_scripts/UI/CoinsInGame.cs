﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsInGame : MonoBehaviour {

    Text text;
    public int coins = 0;  //il numero di monete raccolte durante il gioco

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        text.text = coins.ToString(); //prendi il valore delle monete raccolte durante la partita
	}
}
