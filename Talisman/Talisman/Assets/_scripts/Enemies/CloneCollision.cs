﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//script per enemies che quando vengono colpiti dalla copia la distruggono
public class CloneCollision : MonoBehaviour
{

    GameObject copia;
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player= GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        copia= GameObject.FindWithTag("Copia");
    }


    void OnCollisionEnter2D(Collision2D col)	//classe per fare in modo che se sbatte contro la copia la distrugge
    {
	    if(col.gameObject == copia && copia.GetComponent<DragThrow>().thrown)
	    {
            player.GetComponent<LivesAndCloneSpawning>().StopWaiter();
            player.GetComponent<LivesAndCloneSpawning>().allowed = true;
            Destroy(GameObject.FindWithTag("Copia"));
        }
    }
}
