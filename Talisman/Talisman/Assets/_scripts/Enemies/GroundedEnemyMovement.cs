﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundedEnemyMovement : MonoBehaviour
{

    public int dir = 1;  //direzione dove cammina (durante il runtime varia da 1 a -1)
    public float speed = 5f;
    public int maxTick = 25;     //unità di misura da decrementare per movimento
    int tick;  //tic durante il runtime
    private SpriteRenderer sr;
    // Use this for initialization
    void Start()
    {
        tick = maxTick;
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (tick == 0)   //fine della corsa
        {
            reset();
        }
        GetComponent<Rigidbody2D>().velocity = new Vector2(speed * dir, GetComponent<Rigidbody2D>().velocity.y);   //muoviti
        tick--; //decrementa i tick
    }

    //resetta i tick e cambia direzione di movimento
    void reset()
    {
        tick = maxTick;
        dir = -dir;
        Flip(-dir);
        GetComponent<Rigidbody2D>().velocity = new Vector2();   //fermati
    }

    public void Flip(int side)
    {       
        bool state = (side == 1) ? false : true;
        sr.flipX = state;
    }
}
