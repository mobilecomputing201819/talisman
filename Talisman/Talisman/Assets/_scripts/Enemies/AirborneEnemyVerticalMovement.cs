﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirborneEnemyVerticalMovement : MonoBehaviour {

    public float dir = 1f;  //direzione dove cammina (durante il runtime varia da 1 a -1)
    public float speed = 5f;
    public int maxTick = 25;     //unità di misura da decrementare per movimento
    public int tick;  //tic durante il runtime

    // Use this for initialization
    void Start () {
        tick = maxTick;
        GetComponent<Rigidbody2D>().gravityScale = 0;
    }


    // Update is called once per frame
    void Update()
    {
        if (tick == 0)   //fine della corsa
        {
            reset();
        }
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed * dir) ;   //muoviti
        tick--; //decrementa i tick
    }

    //resetta i tick e cambia direzione di movimento
    void reset()
    {
        tick = maxTick;
        dir = -dir;
        GetComponent<Rigidbody2D>().velocity = new Vector2();   //fermati
    }
}
