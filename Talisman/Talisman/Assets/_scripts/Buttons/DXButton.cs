﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DXButton : MonoBehaviour {

    public void OnClick()
    {
        GameObject.FindWithTag("Player").GetComponent<Movement>().pressMove = 1f;
        if (GameObject.FindWithTag("Copia"))
        {
            GameObject.FindWithTag("Copia").GetComponent<DragThrow>().buttonPressed = true;
        }
    }

    public void OnRelease()
    {
        GameObject.FindWithTag("Player").GetComponent<Movement>().pressMove = 0f;
        if (GameObject.FindWithTag("Copia"))
        {
            GameObject.FindWithTag("Copia").GetComponent<DragThrow>().buttonPressed = true;
        }
    }
}
