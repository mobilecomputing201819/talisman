﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JUMPButton : MonoBehaviour {

    public void OnClick()
    {
        GameObject.FindWithTag("Player").GetComponent<Movement>().pressJump = true;
        if (GameObject.FindWithTag("Copia") != null)
        {
            GameObject.FindWithTag("Copia").GetComponent<DragThrow>().buttonPressed = true;
        }
    }

    public void OnRelease()
    {
        if (GameObject.FindWithTag("Copia"))
        {
            GameObject.FindWithTag("Copia").GetComponent<DragThrow>().buttonPressed = true;
        }
    }
}
