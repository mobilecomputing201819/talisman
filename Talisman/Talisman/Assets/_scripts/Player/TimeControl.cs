﻿using UnityEngine;

public class TimeControl : MonoBehaviour {

    public float slowDownFactor = 0.05f;
    public float slowDownLenght = 4f;


    private void Update()
    {
        Time.timeScale += (1f / slowDownLenght) * Time.unscaledDeltaTime;
        Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
    }

    public void DoSlowmotion ()
    {
        Time.timeScale = slowDownFactor;
        Time.fixedDeltaTime = Time.timeScale * .02f;
    }
}