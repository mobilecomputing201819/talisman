﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivesAndCloneSpawning : MonoBehaviour
{

    public GameObject copia;
    public GameObject copiaUI;   //l'elemento ui che mostra che è possibile spawnare la copia
    public GameObject SlowMotionController;
    public bool allowed = true;     //per prevenire errori
    public int lifeSpan = 4;    //tempo di vita della copia
    public bool beenGrounded = true;    //variabile per prevenire errori, diventa false se thrown è true e torna ad essere true una volta che (quando thrown è false) tocca un muro o il pavimento
    bool thrown;    //variabile booleana che verifica da CloneCollision se il player è sbalzato
    public GameObject DespawnPoint;
    [HideInInspector]
    public Coroutine lastCoroutine = null;
    
    public bool hasKilled;

    public int lives = 3;   //vite del player
    public float smack = 1f;    //di quanto viene sbalzato se prende danno

    public float invulnerable = 1f; //se colpito una volta, lasciagli il tempo di levarsi
    public bool isInvulnerable = false;    //per prevenire che venga colpito
    float timer = 0f;   //per vedere quando può venir colpito

    private Animator anim;
    private Rigidbody2D rb;
    public AudioSource audioSource;
    public AudioClip DamageTakenClip;
    public AudioClip MakeCopyClip;
    public AudioClip CollisionClip;
    public AudioClip KillClip;
    public AudioClip CoinClip;
    public GameObject endGame;
    
    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        if (copia == null)
        {
            allowed = false;
        }
        copiaUI = GameObject.FindWithTag("CopiaUI");
    }

    // Update is called once per frame
    void Update()
    {
        thrown = GetComponent<PlayerThrower>().thrown;  //prendi thrown da CloneCollider

        if (GetComponent<Movement>().hasDoubleJumped && allowed && beenGrounded)    //se è permesso spawnare la copia e viene eseguito un doppio salto (non metto hasDowbleJumped perché crea bug
        {
            allowed = false;
            //Time.timeScale = 1;
            audioSource.clip = MakeCopyClip;
            audioSource.Play();
            Instantiate(copia, new Vector2(transform.position.x, transform.position.y- 1), Quaternion.identity);    //se preme Q quando non ci sono copie, creane una
            copiaUI.GetComponent<SpriteRenderer>().enabled = false; //non è più visibile l'icona di copiaUI
            beenGrounded = false;                       
            GetComponent<PlayerThrower>().thrown = false;
            lastCoroutine = StartCoroutine(waiter());   //attendi (lifeSpan) secondi
        }

        if (allowed && beenGrounded)    //se è permesso spawnare la copia, mostralo tramite copiaUI
        {
            copiaUI.GetComponent<SpriteRenderer>().enabled = true;
        }

        if (lives == 0)  //se il player ha esaurito le vite, ferma il gioco
        {           
            rb.velocity = new Vector2(0, 5f);        
            GetComponent<CapsuleCollider2D>().enabled = false;
           // rb.AddForce(Vector2.up);
            //GameObject.FindWithTag("MainCamera").GetComponent<CameraMovement>().enabled = false;    //blocca la telecamera nell'endless
            StartCoroutine("Kill");
           // Destroy(gameObject);    //distruggi il player           
        }

        if (isInvulnerable)    //una funzione IEnumerator non funzionava, quindi ho provato questo
        {
            timer += Time.deltaTime;
            if (timer > invulnerable)  //scadenza dell'invulnerabilità
            {
                isInvulnerable = false;
            }
        }

        if (transform.position.y < DespawnPoint.transform.position.y) StartCoroutine("Kill");


        
    }

    

    //attendi n secondi ed elimina la copia se non è stata lanciata verso il player
    IEnumerator waiter()
    {
        yield return new WaitForSeconds(lifeSpan);
        Destroy(GameObject.FindWithTag("Copia"));        
        allowed = true;
    }

    private void OnCollisionEnter2D(Collision2D col)    //quando entra in collisione con la copia durante un lancio, distruggila
    {
        //se sbatte contro un nemico
        if (col.gameObject.tag == "Enemy")
        {
            if (!(thrown || isInvulnerable))    //se non sbatte contro il nemico in seguito ad un lancio e se non ha già subito danno recentemente, decrementa la vita e rendilo invulnerabile per un po' di tempo
            {
                rb.velocity = new Vector2(0, 0);   //bloccati per lo sbalzo
                audioSource.clip = DamageTakenClip;
                audioSource.Play();
                StartCoroutine(DisableMovement(0.2f)); //per ogni forza che viene applicata al personaggio ricorda di disabilitarne il movimento in modo da non avere accumuli
                isInvulnerable = true;
                rb.AddForce(((col.gameObject.transform.position - gameObject.transform.position) * smack * 8) * -(GetComponent<Movement>().side));  //sbalzalo
                lives = lives - 1;
                //timer = 0f;
            }
            if (thrown)
            {
                GetComponent<Movement>().hasDoubleJumped = false;
                anim.SetBool("Thrown", false);
                GetComponent<PlayerThrower>().StopBounce(); //interrompi la sequenza di rimbalzi 
                StopCoroutine("DisableMovement");
                GetComponent<ConstantForce2D>().force = new Vector2(0, 0);
                StartCoroutine(DisableMovement(0.2f));
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);   //bloccati per il saltino
                audioSource.clip = KillClip;
                audioSource.Play();
                Destroy(col.gameObject);
                StopCoroutine(lastCoroutine);
                //fagli fare un saltino
                //rb.AddForce(new Vector2(0, 1000));
                rb.velocity = Vector2.up *6;              
                allowed = true; //è permesso spawnare la copia
                GetComponent<PlayerThrower>().thrown = false; //non è più in stato di lancio
                //è possibile rievocare la copia
               
                allowed = true;
                beenGrounded = true;
                SlowMotionController.GetComponent<TimeControl>().DoSlowmotion();
                
             }
        }

        if(col.gameObject.tag == "trap")
        {
            rb.velocity = new Vector2(0, 0);   //bloccati per lo sbalzo
            audioSource.clip = DamageTakenClip;
            audioSource.Play();
            StartCoroutine(DisableMovement(0.2f)); //per ogni forza che viene applicata al personaggio ricorda di disabilitarne il movimento in modo da non avere accumuli
            isInvulnerable = true;
            rb.AddForce(((col.gameObject.transform.position - gameObject.transform.position) * smack * 8) * -(GetComponent<Movement>().side));  //sbalzalo
            lives = lives - 1;
        }
        //se sbatte contro il muro e non è stato lanciato
     /*   if (col.gameObject.tag != "Copia" && col.gameObject.tag != "Enemy" && !thrown)
        {
            beenGrounded = true;    //può effettuare un nuovo doppio salto per evocare la copia
        } */
        
        if(col.gameObject.tag != "Copia" && col.gameObject.tag != "Enemy" && thrown)
        {
            audioSource.clip = CollisionClip;
            audioSource.Play();
        }      

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Coin")
        {
            audioSource.clip = CoinClip;
            audioSource.Play();
        }
    }

    //per dargli il tempo di allontanarsi da mostri
    IEnumerator invulnerability()
    {
        yield return new WaitForSeconds(invulnerable);
        isInvulnerable = false; //ha avuto abbastanza tempo per togliersi
    }

    public IEnumerator DisableMovement(float time)
    {
        GetComponent<Movement>().canMove = false;
        yield return new WaitForSeconds(time);
        GetComponent<Movement>().canMove = true;
    }

    //funzione di appoggio per gli script esterni per fermare waiter
    public void StopWaiter()
    {
        StopCoroutine("waiter");
    }

    public IEnumerator Kill()
    {
        StartCoroutine(GetComponent<Movement>().DisableMovement(4f));
        yield return new WaitForSeconds(3f);
        Destroy(gameObject);
        // Faccio apparire la schermata EndGame
        endGame.SetActive(true);
    }
}