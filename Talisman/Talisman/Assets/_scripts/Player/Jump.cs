﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Jump : MonoBehaviour
{
   
    private bool isGrounded;
    
    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;
    public float jumpForce = 300.0f;
    public bool doubleJump;
    public float maxSpeed = 7.0f;
    public bool jumped = false;
    //private IEnumerator coroutine;



    Animator anim;
    Rigidbody2D rb;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        //coroutine = Wait();
       
    }

    //Simple Jump / DoubleJump feature, uses isGrounded from playerController script
    private void FixedUpdate()
    {
       
    }
    


    //Methods for changing sprites and enabling doubleJump
    private void OnCollisionStay2D(Collision2D collision)
    {     
        jumped = false;
        doubleJump = true;
        anim.SetFloat("Height", 0);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
            jumped = true;
            anim.SetFloat("Height", 1);      
    }

    //Quite Self-explainatory, Creates a vector with the velocity of the Horizontal Movement while adding the constant "jumpForce" to the Y-Axes
    public void jump()
    {
        rb.velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
        rb.AddForce(new Vector2(0, jumpForce));
    }

    private void Update()
    {
        if (rb.velocity.y < 0 && jumped)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime; // -1 perchè unity già aggiunge tutto per 1
        }

        else if (rb.velocity.y > 0 && !Input.GetButton("Jump") && jumped)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }
    }

    public void wallJump()
    {
            doubleJump = true;
            if (GetComponent<PlayerController>().isFacingRight && !GetComponent<PlayerController>().IsGrounded())
            {          
            rb.AddForce(new Vector2(-jumpForce-100, 1));         
            }          
    }

    /*IEnumerator Wait()
    {
        yield return new WaitForSeconds(10);
    }*/
}

