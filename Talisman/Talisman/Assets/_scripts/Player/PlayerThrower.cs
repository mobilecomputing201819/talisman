﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerThrower : MonoBehaviour {

    public float speedModifier = 1f;   //moltiplicatore dello "sbalzo"
    public float time = 0.5f;    //quanto tempo deve essere lo script di rimbalzo
    public bool thrown; //il player è in fase di lancio?
    
    float speed;   //variabile da "copiare" dal dragthrow della copia
    float grav; //variabile in cui salvare la gravità del personaggio

    private Animator anim;
    private Rigidbody2D rb;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {

	}

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Copia" && col.gameObject.GetComponent<DragThrow>().thrown)
        {
            StopCoroutine("waiter");
            //modifica la gravità e salvala in una variabile
            grav = rb.gravityScale;
            rb.gravityScale = 0;

            //copia le variabili dal dragthrow
            speed = col.gameObject.GetComponent<DragThrow>().speed * speedModifier;
            GetComponent<TrailRenderer>().enabled = true;
            //fai tutte le cose delle componenti
            GetComponent<Movement>().hasJumped = true; // altrimenti non può fare il doppio salto dopo
            GetComponent<CapsuleCollider2D>().enabled = false;
            GetComponent<CircleCollider2D>().enabled = true;
            GetComponent<ConstantForce2D>().force = speed * col.gameObject.GetComponent<DragThrow>().fixedLaunch(gameObject.transform.position - col.gameObject.transform.position);    //imponi una forza costante per il lancio
            // rb.AddForce((speed * col.gameObject.GetComponent<DragThrow>().fixedLaunch(gameObject.transform.position - col.gameObject.transform.position) * throwForce));    //imponi una forza costante per il lancio           
            Destroy(col.gameObject);    //distruggi la copia

            //cambia le variabili booleane
            thrown = true;
            anim.SetBool("Thrown", true);
            StartCoroutine(GetComponent<LivesAndCloneSpawning>().DisableMovement(time + 0.2f)); //inserita una coroutine che rende falso il booleano canMove di Movement, risolvendo il bug delle forze sommate
            StartCoroutine(bounce(time));
        }
    }
    private void OnCollisionEnter2D(Collision2D collider)
    {
        //se sbatte contro il muro
        if (collider.gameObject.tag != "Copia" && collider.gameObject.tag != "Enemy" && thrown)
        {
            GetComponent<ConstantForce2D>().force = Vector2.Reflect(GetComponent<ConstantForce2D>().force, collider.GetContact(0).normal);   //fallo rimbalzare
        }
    }

//dopo quanto smette di rimbalzare
IEnumerator bounce(float t)
    {
        yield return new WaitForSeconds(t);
        rb.velocity = Vector2.Lerp(new Vector2(0, 0), rb.velocity, 0.2f); // annullo la velocità in modo che, nel momento in cui può muoversi, non ci siano forse accumulate
        rb.gravityScale = grav;    //resetta la gravità

        //azzera forza costante nel caso non abbia sbattuto da nessuna parte e lavora su tutte le altre componenti
        GetComponent<ConstantForce2D>().force = new Vector2();
        GetComponent<CapsuleCollider2D>().enabled = true;
        GetComponent<CircleCollider2D>().enabled = false;
        GetComponent<TrailRenderer>().enabled = false;

        //cambia variabili booleane 
        anim.SetBool("Thrown", false);
        thrown = false;
        GetComponent<LivesAndCloneSpawning>().allowed = true;
    }

    //funzione di appoggio per script esterni
    public void StopBounce()
    {
        StopCoroutine("bounce");
    }

}
