﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Movement : MonoBehaviour
{
    private Collision coll;
    [HideInInspector]
    public Rigidbody2D rb;
    private AnimationScript anim;
    private Animator animator;

    //valori per i pulsanti
    public float pressMove;
    public bool pressJump;

    [Space]
    [Header("Stats")]
    public float speed = 10;
    public float jumpForce = 13;
    public float slideSpeed = 4;
    public float wallJumpLerp = 10;

    [Space]
    [Header("Booleans")]
    public bool canMove;
    public bool wallJumped;
    public bool wallSlide;
    public bool pushingWall;
    public bool hasJumped;
    public bool hasDoubleJumped;
    public bool squeeze = false;

    [Space]
    [Header("Audio Manager")]
    public AudioSource audioSource;
    public AudioClip JumpClip;

    [Space]
    [Header("Buttons")]
    public GameObject JumpButton;

    [Space]
    private bool groundTouch;
    public int side = 1;

    // Start is called before the first frame update
    void Start()
    {
        coll = GetComponent<Collision>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<AnimationScript>();
        animator = GetComponent<Animator>();
        canMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        float x= Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");
        if (pressMove != 0)
        {
            x = pressMove;
            y = 0;
        }
        Vector2 dir = new Vector2(x, y);
        Walk(dir);
        anim.SetHorizontalMovement(x, 0, rb.velocity.y);


        if (coll.onGround && canMove)
        {
            hasJumped = false;
            animator.SetFloat("Speed", Mathf.Abs(x));                       
            wallSlide = false;
            GetComponent<LivesAndCloneSpawning>().beenGrounded = true;
            anim.SetTrigger("Grounded");
            wallJumped = false;
            pushingWall = false;          
            hasDoubleJumped = false;
            squeeze = false;
        }                 

        if((coll.onGround || coll.onWall) && !GetComponent<PlayerThrower>().thrown)
        {
            GetComponent<CircleCollider2D>().enabled = false;
            GetComponent<CapsuleCollider2D>().enabled = true;
        }

        if (!coll.onWall || coll.onGround)
        {
            animator.SetBool("WallSliding", false);
            wallSlide = false;
        }

        if(coll.onWall && !coll.onGround)
        {
            if (x != 0 || pushingWall)
            {
                wallSlide = true;
                hasDoubleJumped = false;
                WallSlide();
            }
            else if (x == 0 && !pushingWall)
            {
                pushingWall = false;
                animator.SetBool("WallSliding", false);
                anim.SetTrigger("Jump");
            }
        }

        if (!coll.onWall && !coll.onGround)
            anim.SetTrigger("Jump");

        if (pressJump || Input.GetButtonDown("Jump"))
        {
            Jump();
            pressJump = false;
            /* audioSource.clip = JumpClip;
             if (hasJumped && !hasDoubleJumped)
             {
                 audioSource.Play();
                 Jump(Vector2.up * 1.2f, false);
                 hasDoubleJumped = true;
             }
             if (coll.onGround && !hasJumped)
             {
                 audioSource.Play();
                 Jump(Vector2.up, false);
                 hasJumped = true;
             }
             if (coll.onWall && !coll.onGround)
             {
                 audioSource.Play();
                 pushingWall = false;
                 WallJump();
                 hasJumped = true;
                 hasDoubleJumped = false;
             }    */
        }   
         
        if (wallSlide || !canMove)
            return;

        if(x > 0)
        {
            side = 1;
            anim.Flip(side);
        }
        if (x < 0)
        {
            side = -1;
            anim.Flip(side);
        }
    }    

    void Jump()
    {
        audioSource.clip = JumpClip;
        //avvia la slowmo
        if(hasDoubleJumped && GameObject.FindWithTag("Copia") != null)
        {
            GameObject.FindWithTag("Copia").GetComponent<SlowMotionController>().pressJump = true;
        }
        if (hasJumped && !hasDoubleJumped)
        {
            audioSource.Play();
            Jump(Vector2.up * 1.2f, false);
            hasDoubleJumped = true;
        }
        if (coll.onGround && !hasJumped)
        {
            audioSource.Play();
            Jump(Vector2.up, false);
            hasJumped = true;
        }
        if (coll.onWall && !coll.onGround)
        {
            audioSource.Play();
            pushingWall = false;
            WallJump();
            hasJumped = true;
            hasDoubleJumped = false;
        }
    }


    private void WallJump()
    {
        if ((side == 1 && coll.onRightWall) || side == -1 && !coll.onRightWall)
        {
            side *= -1;
            anim.Flip(side);
        }
        animator.SetBool("WallJump", true);
        StopCoroutine(DisableMovement(0));
        StartCoroutine(DisableMovement(.1f));

        Vector2 wallDir = coll.onRightWall ? Vector2.left : Vector2.right;

        Jump((Vector2.up / 1.5f + wallDir / 1.5f) *1.5f, true);

        wallJumped = true;
    }

    private void WallSlide()
    {
        
        if (coll.wallSide != side)
            anim.Flip(side * -1);

        if (!canMove)
            return;

        if ((rb.velocity.x > 0 && coll.onRightWall && pressMove != 0) || (rb.velocity.x < 0 && coll.onLeftWall && pressMove != 0))
        {
            pushingWall = true;
            animator.SetBool("WallSliding", true);
        }
        
        float push = pushingWall ? 0 : rb.velocity.x;
        
        rb.velocity = new Vector2(push, -slideSpeed);
    }

   

    private void Walk(Vector2 dir)
    {
        if (!canMove)
            return;

        if (!wallJumped)
        {
            rb.velocity = new Vector2(dir.x * speed, rb.velocity.y);
        }
        else
        {
            rb.velocity = Vector2.Lerp(rb.velocity, (new Vector2(dir.x * speed, rb.velocity.y)), wallJumpLerp * Time.deltaTime);
        }
    }

    public void Jump(Vector2 dir, bool wall)
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.velocity += dir * jumpForce;    
    }

    public IEnumerator DisableMovement(float time)
    {
        canMove = false;
        yield return new WaitForSeconds(time);
        canMove = true;
    }

    void RigidbodyDrag(float x)
    {
        rb.drag = x;
    }

    }

