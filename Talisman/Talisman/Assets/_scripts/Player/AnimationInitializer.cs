﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationInitializer : MonoBehaviour {
    private Collision coll;
    private Animator anim;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        coll = GetComponent<Collision>();
	}
	
	// Update is called once per frame
	void Update () {
        if (coll.onGround)
        {
            anim.SetFloat("Speed",Mathf.Abs(Input.GetAxisRaw("Horizontal")));
        }
	}
}
