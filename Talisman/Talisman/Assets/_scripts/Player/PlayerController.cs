﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    [HideInInspector]
    public bool isFacingRight = true;
    [HideInInspector]
    public bool isJumping = false;
    [HideInInspector]
    
    private float rayCastHorizontal = 0.36f;
   
    public LayerMask groundLayer;
    public float maxSpeed = 7.0f;
    Animator anim;
    Rigidbody2D rb;

    // Use this for initialization
    void Start()
    {       
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {      
    }

    //Horizontal Movement plus sprite flipping
    void FixedUpdate()
    {
        try
        {
            this.moveCharacter();
        }
        catch (UnityException error) { Debug.LogError(error.ToString()); }
        //Movement
/*float xaxis = Input.GetAxisRaw("Horizontal") * 7f;

        //Move with X = Left right
        Vector2 VelocityX = rb.velocity;
        VelocityX.x = xaxis;
        rb.velocity = VelocityX;

        //Max Movement spd
        if (rb.velocity.magnitude > 7f)
        {
            rb.velocity *= 7f / rb.velocity.magnitude;
        } */
    }

    //Boolean Function to be used for mevement
    public bool IsGrounded()
    {
        Vector2 position = transform.position;
        Vector2 direction = Vector2.down;
        float distance = 0.7f;

        RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, groundLayer);
        if (hit.collider != null)
        {
            return true;
        }

        return false;
    }

    public bool isWalled()
    {
        Vector2 position = transform.position;
        RaycastHit2D hitRight = Physics2D.Raycast(position, Vector2.right, rayCastHorizontal, groundLayer);
        RaycastHit2D hitLeft = Physics2D.Raycast(position, Vector2.left, rayCastHorizontal, groundLayer);

        if(hitRight.collider != null || hitLeft.collider != null)
        {
            return true;
        }
        return false;
    }

    //Flipping character sprite to match direction in the FixedUpdate
    void flip()
    {
        isFacingRight = !isFacingRight;
        Vector3 playerScale = transform.localScale;
        playerScale.x = playerScale.x * -1;
        transform.localScale = playerScale;
    }

    void moveCharacter()
    {
        if (Input.GetAxis("Horizontal") != 0)
        {
            float move = Input.GetAxisRaw("Horizontal");
            anim.SetFloat("Speed", Mathf.Abs(move));
            rb.velocity = new Vector2(move*maxSpeed, rb.velocity.y);
            if ((move > 0.0f && isFacingRight == false) || (move < 0.0f && isFacingRight == true))
            {
                flip();
            }
        }
        if (Input.GetButtonDown("Jump"))
        {
            if (IsGrounded())
                GetComponent<Jump>().jump();

            if (IsGrounded() == false && GetComponent<Jump>().doubleJump)
            {
                GetComponent<Jump>().doubleJump = false;
                GetComponent<Jump>().jump();
            }
            if (isWalled() && !IsGrounded())
            {              
                GetComponent<Jump>().wallJump();
            }
        }
    }
}

