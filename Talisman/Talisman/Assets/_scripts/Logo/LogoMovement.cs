﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoMovement : MonoBehaviour {

    public float speed = 1;  //valore y del movimento
    int tick;   //unità di tempo
    public int maxTick = 10;   //dopo quanti tick cambia direzione

    Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        rb.velocity = new Vector2(0, speed);    //muoviti
        tick++; //incrementa i tick

        if (tick >= maxTick)    //quando è ora di cambiare direzione
        {
            speed = -speed;
            tick = 0;
        }
	}
}
