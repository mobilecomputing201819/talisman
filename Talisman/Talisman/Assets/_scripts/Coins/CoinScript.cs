﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour {

    GameObject coinUI;

    private void Start()
    {
        coinUI = GameObject.FindWithTag("CoinUI");
    }

    //quando il player entra in collisione con la moneta, incrementa il valore sullo schermo e distruggila
    void OnTriggerEnter2D(Collider2D col)
    {       
        if (col.gameObject.tag == "Player")
        {          
            PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + 1);   //salva una moneta aggiuntiva
            coinUI.GetComponent<CoinsInGame>().coins = coinUI.GetComponent<CoinsInGame>().coins + 1;    //aumenta il numero di monete da mostrare sullo schermo in gioco
            
            Destroy(gameObject);
        }
    }
}
