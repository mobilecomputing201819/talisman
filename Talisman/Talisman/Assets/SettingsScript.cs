﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SettingsScript : MonoBehaviour
{
    public AudioMixer masterMixer;

    public void SetMusicVolume(float musicVol)
    {
        // I set the audio mixer's exposed variabile musicVol to the value musicVol (the variable of our music slider)
        masterMixer.SetFloat("musicVol", musicVol);
    }

    public void SetSoundEffectsVolume(float soundEffectsVol)
    {
        // I set the audio mixer's exposed variabile soundEffectsVol to the value soundEffectsVol (the variable of our sound effects slider)
        masterMixer.SetFloat("soundEffectsVol", soundEffectsVol);
    }
}