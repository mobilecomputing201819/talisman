﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDespawner : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(despawner());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator despawner()
    {
        yield return new WaitForSeconds(3);
        Destroy(gameObject);
    }
}
