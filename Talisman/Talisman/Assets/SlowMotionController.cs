﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMotionController : MonoBehaviour
{

    public GameObject TimeControl;
    public bool isThrown;
    public bool isAllowed;
    public bool pressJump;  //variabile a cui accede il pulsante jump 
    // Use this for initialization
    void Start()
    {
        isAllowed = true;
    }

    // Update is called once per frame
    void Update()
    {

        isThrown = GetComponent<DragThrow>().thrown;

         if (pressJump && !isThrown && isAllowed && !GameObject.FindGameObjectWithTag("Player").GetComponent<Collision>().onGround)
         {
             isAllowed = false;
            pressJump = false;
             TimeControl.GetComponent<TimeControl>().DoSlowmotion();
         } 

         if (isThrown)
         {
             Time.timeScale = 1;
         }

     }  
    }

