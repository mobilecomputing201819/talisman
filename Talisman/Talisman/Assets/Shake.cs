﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shake : MonoBehaviour
{
    Vector3 originalPosition;
    // Di quanto si posta la Main Camera
    float magnitude = 0.2f;

    public IEnumerator CameraShake()
    {
        // Posizione iniziale Main Camera
        originalPosition = transform.localPosition;

        // Mi sposto a sinistra
        transform.localPosition = new Vector3(originalPosition.x , originalPosition.y - magnitude, originalPosition.z);

        // Go to the next frame
        yield return null;

        // Reimposto la posizione iniziale
        transform.localPosition = originalPosition;

        // Go to the next frame
        yield return null;

        // Mi sposto a destra
        transform.localPosition = new Vector3(originalPosition.x , originalPosition.y + magnitude, originalPosition.z);

        // Go to the next frame
        yield return null;

        // Reimposto la posizione iniziale
        transform.localPosition = originalPosition;
    }
}