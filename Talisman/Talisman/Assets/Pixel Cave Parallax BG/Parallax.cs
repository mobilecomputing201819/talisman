﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour {

    private float lenght, startpos;
    public GameObject camera;
    public float parallaxEffect;
	// Use this for initialization
	void Start () {
        startpos = transform.position.y;
        lenght = GetComponent<SpriteRenderer>().bounds.size.y;
	}
	
	// Update is called once per frame
	void Update () {
        float temp = (camera.transform.position.y * (1 - parallaxEffect));
        float dist = (camera.transform.position.y * parallaxEffect);

        transform.position = new Vector3(transform.position.x, startpos + dist, transform.position.z);

        if (temp > startpos + lenght) startpos += lenght;
        else if (temp < startpos - lenght) startpos += lenght;
	}
}
