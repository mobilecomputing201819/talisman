﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadScene : MonoBehaviour
{
    public Slider slider;
    public Text progressText;

    public void ChangeScene(string sceneName)
    {
        // Because this method is running asynchronously, meaning as a Coroutine, we'll have to do that in a Coroutine as well
        StartCoroutine(LoadAsynchronously(sceneName));
    }

    IEnumerator LoadAsynchronously(string scene)
    {
        // LoadSceneAsync loads the scene asynchronously in the background, that means that it keeps our current scene and all of the behaviors in it running while it's loading our new scene into memory
        // What we can do is get some information back from our scene manager about the progress of this operation
        // LoadSceneAsync method returns an object with information about how our operation is going, this object is called AsyncOperation
        AsyncOperation operation = SceneManager.LoadSceneAsync(scene);

        while (!operation.isDone)
        {
            // I'm using a bit of math in order to make our operation.progress value from 0 to 0.9 go from 0 to 1
            float progress = Mathf.Clamp01(operation.progress / 0.9f);

            slider.value = progress;
            progressText.text = progress * 100f + "%";

            // Wait until the next frame before continuing
            yield return null;
        }
    }
}