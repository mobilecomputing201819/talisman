﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public GameObject damageEffect;
    public GameObject smokeCloud;

    private Vector2 playerPosition;
    private Vector2 enemyPosition;

    public Shake shake;
    public bool isVulnerable;
    public bool isThrown;
    void Update()
    {
        isVulnerable = !GetComponent<LivesAndCloneSpawning>().isInvulnerable;
        isThrown = GetComponent<PlayerThrower>().thrown;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isVulnerable && (collision.collider.tag == "Enemy" || collision.collider.tag == "trap"))
        {
            // Scuoti telecamera
           StartCoroutine(shake.CameraShake());

            // Salvo la posizione del player e del nemico toccato in modo che i particellari appaiano nel punto giusto
            playerPosition = gameObject.transform.position;            

            // Istanzia i due particle systems
            Instantiate(damageEffect, playerPosition, Quaternion.identity);                                
        }

        if (isThrown && collision.collider.tag == "Enemy")
        {
            StartCoroutine(shake.CameraShake());

            enemyPosition = collision.collider.transform.position;

            Instantiate(smokeCloud, enemyPosition, Quaternion.identity);
        }
    }
}