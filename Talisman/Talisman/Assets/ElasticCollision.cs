﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElasticCollision : MonoBehaviour
{
    private Rigidbody2D rb;
    private Vector2 lastVelocity;

    public float speed = 20f;
    private Vector2 direction;

    public float setTime = 3f;
    private bool timeEnded = false;
    public float smooth = 10f;

    private float jumpForce = 13f;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<CircleCollider2D>().enabled = true;
    }

    private void Update()
    {
        // When the Coroutine ends the speed decreases quickly until it reaches zero
        if (timeEnded)
        {
            rb.velocity = Vector2.Lerp(rb.velocity, Vector2.zero, smooth * Time.deltaTime);
            rb.isKinematic = false;
            GetComponent<CircleCollider2D>().enabled = false;
            GetComponent<BoxCollider2D>().enabled = true;

            // Enable Movement script
            GetComponent<Movement>().enabled = true;
        }
    }

    private void FixedUpdate()
    {
        // Save velocity for use after a collision
        lastVelocity = rb.velocity;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Copia")
        {
            DragThrow cloneScript = collision.collider.gameObject.GetComponent<DragThrow>();

            if (cloneScript.thrown) // If the player collides with the clone that was thrown at him
            {
                // Setting rb.isKinematic the rigidbody will be under full control of script
                // Checking rb.useFullKinematicContacts allows kinematic/kinematic and kinematic/static collisions, without this the collision between the copy and the player both kinematic would not be recorded
                rb.isKinematic = true;
                rb.useFullKinematicContacts = true;

                // Disable Movement script
                GetComponent<Movement>().enabled = false;

                float x = transform.position.x - collision.transform.position.x;
                float y = transform.position.y - collision.transform.position.y;

                // Calculate direction, make length = 1 via .normalized
                direction = new Vector2(x, y).normalized;

                rb.velocity = direction * speed;

                StartCoroutine(StopBall());
            }
        }

        // Reflect velocity off the surface
        if (collision.collider.tag != "Copia" && collision.collider.tag != "Enemy")
        {
            Vector2 surfaceNormal = collision.GetContact(0).normal;
            rb.velocity = Vector2.Reflect(lastVelocity, surfaceNormal);
        }

        if (collision.collider.tag == "Enemy")
        {
            //Destroy(collision.collider.gameObject);

            rb.velocity = Vector2.zero;

            rb.isKinematic = false;

            Jump(Vector2.up);
            //rb.isKinematic = false;
        }
    }

    private void Jump(Vector2 dir)
    {
        rb.velocity += dir * jumpForce;
    }

    IEnumerator StopBall()
    {
        yield return new WaitForSeconds(setTime);

        timeEnded = true;
    }
}